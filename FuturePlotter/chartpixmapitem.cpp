#include "chartpixmapitem.h"

#include <QGraphicsSceneMouseEvent>

ChartPixmapItem::ChartPixmapItem(const QPixmap & pixmap, QWidget *mainWidget)
    : QGraphicsPixmapItem(pixmap), m_mainWidget {mainWidget}
{
    m_image = this->pixmap().toImage();
}

void ChartPixmapItem::setProbing(bool yes)
{
    m_probing = yes;
}

void ChartPixmapItem::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
{
    if (m_probing)
        //emit colorProbed(m_image.pixelColor(event->pos().x(), event->pos().y()));
        return;

    return QGraphicsPixmapItem::mouseMoveEvent(event);
}
