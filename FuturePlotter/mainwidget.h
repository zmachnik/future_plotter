#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QImage>
#include <QProcess>

#include <chartpixmapitem.h>

#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWidget; }
QT_END_NAMESPACE

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget * parent = nullptr);
    ~MainWidget();

protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;

private:
    Ui::MainWidget *ui;

    QGraphicsScene m_scene;
    QGraphicsPixmapItem *m_pixmapItem = nullptr;
    QImage m_chart, m_originalChart;
    bool m_selectingDataSeries = false;
    QColor m_dataSeriesColor;
    QPixmap m_dataSeriesColorIcon;
    int m_firstDataColumn = -1;
    QString m_workingDir;
    QProcess * m_Rscript = nullptr;
    int m_activeModels = 0;

    std::vector<unsigned> m_values;
    std::vector<int> m_forecast;
    std::vector<int> m_low_forecast;
    std::vector<int> m_high_forecast;

    static bool colorsSimilar(const QColor & c1, const QColor & c2, int tolerance);

    QString createRScript();

    void loadImage(const QImage & img);
    void drawData();
    bool readResults();

private slots:
    void selectFile();
    void fromClipboard();
    void updateControls();
    void grabData();
    void runRScript();
    void plotFuture();
    void resetEverything();
    void selectDataSeries(bool yes = true);
    void setProbedColor(QColor color);
};
#endif // MAINWIDGET_H
