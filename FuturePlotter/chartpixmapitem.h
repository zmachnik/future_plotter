#ifndef CHARTPIXMAPITEM_H
#define CHARTPIXMAPITEM_H

#include <QGraphicsPixmapItem>
#include <QMouseEvent>

class ChartPixmapItem : public QGraphicsPixmapItem
{
public:
    ChartPixmapItem(const QPixmap &pixmap, QWidget * mainWidget = nullptr);
    void setProbing(bool yes);

protected:
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QImage m_image;
    bool m_probing = false;
    const QWidget * m_mainWidget;
};

#endif // CHARTPIXMAPITEM_H
