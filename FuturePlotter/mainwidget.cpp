#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QFileDialog>
#include <QMouseEvent>
#include <QScreen>
#include <QDir>
#include <QProcess>
#include <QTextStream>
#include <QClipboard>
#include <QMimeData>
#include <QThread>

MainWidget::MainWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWidget), m_dataSeriesColorIcon{ 32, 32 }
{
    ui->setupUi(this);

    m_workingDir = QDir::currentPath();

    setMouseTracking(true);
    setProbedColor(QColor{"black"});

    updateControls();
}

MainWidget::~MainWidget()
{
    delete ui;
}

void MainWidget::mousePressEvent(QMouseEvent * event)
{
    if (m_selectingDataSeries) {

        if (event->button() == Qt::LeftButton)
            setProbedColor(grab().toImage().pixel(event->pos()));

        selectDataSeries(false);
    }

    else return QWidget::mousePressEvent(event);
}

void MainWidget::mouseMoveEvent(QMouseEvent * event)
{
    return QWidget::mouseMoveEvent(event);
}

bool MainWidget::colorsSimilar(const QColor &c1, const QColor &c2, int tolerance)
{
    const auto t { 255. * (tolerance/100.) };

    if ((c1.red() > c2.red() - t) && (c1.red() < c2.red() + t))
        if ((c1.green() > c2.green() - t) && (c1.green() < c2.green() + t))
            if ((c1.blue() > c2.blue() - t) && (c1.blue() < c2.blue() + t))
                return true;

    return false;
}

void MainWidget::loadImage(const QImage &img)
{
    m_chart = QImage { img.width() * 2, img.height() * 3, img.format() };
    m_chart.fill(Qt::white);

    QPainter painter { &m_chart };
    painter.drawImage(QPoint(0, m_chart.height()/3), img);

    m_pixmapItem = new QGraphicsPixmapItem(QPixmap::fromImage(m_chart));

    m_scene.addItem(m_pixmapItem);
    ui->chartGV->setScene(&m_scene);

    ui->resetPB->setEnabled(true);

    ui->filePB->setEnabled(false);
    ui->clipboardPB->setEnabled(false);

    ui->selectDataPB->setEnabled(true);
    ui->stepsGB->setEnabled(true);
    ui->grabDataPB->setEnabled(true);

    updateControls();
}

void MainWidget::selectFile()
{
    auto fileName { QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp)"))};

    if (!fileName.isEmpty()) loadImage(QImage { fileName });
}

void MainWidget::fromClipboard()
{
    const auto mimeData { QApplication::clipboard()->mimeData() };

    if (mimeData->hasImage())
        loadImage(qvariant_cast<QPixmap>(mimeData->imageData()).toImage());
}

void MainWidget::updateControls()
{
    ui->stepsPercentL->setText(QString::number(ui->stepsHS->value()) + "%");
    ui->levelPercentL->setText(QString::number(ui->levelHS->value()) + "%");

    m_activeModels = 0;
    if (ui->arimaCB->isChecked()) m_activeModels++;
    if (ui->etsCB->isChecked()) m_activeModels++;
    if (ui->tbatsCB->isChecked()) m_activeModels++;
    if (ui->nnetarCB->isChecked()) m_activeModels++;

    ui->weightsGB->setEnabled(m_activeModels > 1);
}

void MainWidget::grabData()
{
    m_firstDataColumn = -1;
    m_forecast.clear();
    m_high_forecast.clear();
    m_low_forecast.clear();
    m_values.clear();
    m_values.reserve(m_chart.width());

    bool dataStarted = false;

    for (int c{0}; c < m_chart.width(); ++c) {

        int first;
        int last;

        bool plotFound = false;
        for (int r{0}; r < m_chart.height(); ++r) {

            if (colorsSimilar(QColor{m_chart.pixel(c,r)}, m_dataSeriesColor, ui->toleranceSB->value())) {
                auto value = m_chart.height()-r;
                if (!plotFound) {
                    plotFound = true;
                    if (!dataStarted) {
                        dataStarted = true;
                        m_firstDataColumn = c;
                    }
                    first = value;
                }
                last = value;
            }
        }
        if (!plotFound) {
            if (dataStarted) break;
            else continue;
        }
        m_values.push_back(((double)first + last) / 2.);
    }

    unsigned drop { (unsigned)m_values.size()
                    * ( 100 - ui->amountSB->value()) / 100 };
    for (unsigned i{0}; i<drop; ++i) m_values.pop_back();

    if (m_values.size()) {
        ui->runRScriptPB->setEnabled(true);
    }

    drawData();
}

void MainWidget::runRScript()
{
    const static QString RPath { "C:/Program Files/R/R-4.0.3/bin/Rscript.exe" }; // TODO

    QFile results {m_workingDir + "/results.txt"};
    results.remove();

    QFile file{m_workingDir + "/forecast.R"};
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(createRScript().toUtf8());

    qint64 pid;
    QProcess::startDetached(QString{"cmd.exe"}, QStringList() << "/k"
                            << RPath << QFileInfo{file}.absoluteFilePath(), "", &pid);

    ui->plotPB->setEnabled(true);
}

void MainWidget::plotFuture()
{
    if (readResults())
        drawData();
}

void MainWidget::resetEverything()
{
    m_scene.clear();

    ui->resetPB->setEnabled(false);

    ui->filePB->setEnabled(true);
    ui->clipboardPB->setEnabled(true);

    ui->selectDataPB->setEnabled(false);
    ui->stepsGB->setEnabled(false);
    ui->grabDataPB->setEnabled(false);
}

void MainWidget::selectDataSeries(bool yes)
{
    if (yes) {
        m_selectingDataSeries = true;
        QApplication::setOverrideCursor(Qt::CrossCursor);
    } else {
        m_selectingDataSeries = false;
        QApplication::setOverrideCursor(Qt::ArrowCursor);
    }
}

void MainWidget::setProbedColor(QColor color)
{
    m_dataSeriesColor = color;
    m_dataSeriesColorIcon.fill(color);
    ui->dataSeriesColorIcon->setPixmap(m_dataSeriesColorIcon);
}

void MainWidget::drawData()
{
    if (m_values.size() && m_firstDataColumn >= 0) {

        QPainter painter(&m_chart);

        QPen pen;
        pen.setWidth(2);
        pen.setColor(colorsSimilar(m_dataSeriesColor, Qt::blue, 15) ? Qt::green : Qt::blue);
        painter.setPen(pen);

        int c = m_firstDataColumn;

        for (unsigned i = 0; i < m_values.size()-1; i++, c++)
            painter.drawLine(c, m_chart.height() - m_values[i],
                             c+1, m_chart.height() - m_values[i+1]);

        painter.end();
    }

    if (m_forecast.size() && m_firstDataColumn >= 0) {

        QPainter painter(&m_chart);

        QPen pen;

        int c = m_firstDataColumn + m_values.size();

        for (unsigned i = 0; (i < m_forecast.size()-1) && (c < m_chart.width()); i++, c++)
        {
            for (int j = m_low_forecast[i+1]; j < m_high_forecast[i+1]; ++j)
            {
                if (j < 1 || j >= m_chart.height()-1) continue;

                QColor pix{m_chart.pixel(c+1, m_chart.height() - j)};
                pix = QColor(pix.red()/1.2, pix.green()/1.2, pix.blue()/1.2);

                pen.setWidth(1);
                pen.setColor(pix);
                painter.setPen(pen);

                painter.drawPoint(c+1, m_chart.height() - j);
            }

            int rFrom {m_chart.height() - (int)m_forecast[i]};
            int rTo {m_chart.height() - (int)m_forecast[i+1]};

            if (rFrom >= 1 && rFrom < m_chart.height()-1 && rTo >= 1
                && rTo < m_chart.height()-1) {
                pen.setWidth(2);
                pen.setColor(colorsSimilar(m_dataSeriesColor, Qt::red, 15) ? Qt::yellow : Qt::red);
                painter.setPen(pen);
                painter.drawLine(c, rFrom, c+1, rTo);
            }
        }


        painter.end();
    }

    m_scene.clear();
    m_pixmapItem = new QGraphicsPixmapItem(QPixmap::fromImage(m_chart));
    m_scene.addItem(m_pixmapItem);
}

bool MainWidget::readResults()
{
    m_forecast.clear();
    m_low_forecast.clear();
    m_high_forecast.clear();

    auto addResult { [&](const QString & line) {
       auto numbers {line.split(";")};
       if (numbers.size() == 3) {
           m_forecast.push_back((int)(numbers.at(0).toDouble() + 0.5));
           m_low_forecast.push_back((int)(numbers.at(1).toDouble() + 0.5));
           m_high_forecast.push_back((int)(numbers.at(2).toDouble() + 0.5));
       }
    }};

    QFile results(m_workingDir + "/results.txt");
    if (results.open(QIODevice::ReadOnly)) {
       QTextStream in(&results);
       while (!in.atEnd()) addResult(in.readLine());
       return true;
    }
    else return false;
}

QString MainWidget::createRScript()
{
//https://cran.r-project.org/web/packages/forecastHybrid/vignettes/forecastHybrid.html

    QString s {

    "library(\"methods\")\n"
    "library(\"forecastHybrid\")\n"
    "#library(\"ggplot2\")\n\n"

    "h.d <- c("
    };

    for (int x : m_values) s += (QString::number(x) + ",");
    s[s.size()-1] = ')';

    s += "\n#autoplot(ts(h.d))\n\n";

    if (m_activeModels == 1) {
         s+= "fc.model <- ";

         if (ui->arimaCB->isChecked()) s += "auto.arima(";
         if (ui->etsCB->isChecked()) s += "ets(";
         if (ui->nnetarCB->isChecked()) s += "nnetar(";
         if (ui->tbatsCB->isChecked()) s += "tbats(";

         s += "h.d)\n";
    }

    else if (m_activeModels > 1) {
         s+= "fc.model <- hybridModel(h.d, models = \"";

         if (ui->arimaCB->isChecked()) s += 'a';
         if (ui->etsCB->isChecked()) s += 'e';
         if (ui->nnetarCB->isChecked()) s += 'n';
         if (ui->tbatsCB->isChecked()) s += 't';

         s += "\", ";

         if (ui->dynamicWeightsRB->isChecked())
         s += "weights = \"cv.errors\", errorMethod = \"RMSE\", ";
         else
         s += "weights = \"equal\", ";

         s += "parallel=TRUE, num.cores=4)\n";
    }

    s += "fc <- forecast(fc.model, ";

    if (m_activeModels == 1 && ui->nnetarCB->isChecked())
        s+= "PI=TRUE, ";

    s += "level=c(";
    s += QString::number(ui->levelHS->value());
    s += "), h = ";

    s += QString::number((unsigned)m_values.size() * ui->stepsHS->value() / 100);

    s +=
    ")\n\n#autoplot(fc)\n\n"
    "write.table(fc, \"";

    s += m_workingDir;

    s+= "/results.txt\", sep = \";\", row.names = FALSE, col.names = FALSE)\n\n"
    "quit(save=\"no\")\n" ;

    return s;
}

