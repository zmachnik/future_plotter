#include "mainwidget.h"

#include <QApplication>
#include <QTextStream>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile stylesheetFile { ":resources/stylesheet.qss" };

    if (stylesheetFile.open(QFile::ReadOnly | QFile::Text))
        a.setStyleSheet(QTextStream {&stylesheetFile}.readAll());

    MainWidget w;
    w.show();
    return a.exec();
}
